<?php
/**
 * Created by IntelliJ IDEA.
 * User: ivo
 * Date: 27/03/2018
 * Time: 20:48
 */

/**
 * Singleton (Design pattern!)
 * Class Database
 */
class DatabaseService
{
    const DB_SERVER = "localhost";
    const DB_USER = "root";
    const DB_PASSWD = "";
    const DB_NAME = "api";

    private $connection = null;

    public static function getInstance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new DatabaseService();
        }
        return $inst;
    }

    /**
     * Private ctor so nobody else can instantiate it
     *
     */
    private function __construct()
    {
    }

    /**
     * Creates a new connection with a database.
     * @return mysqli connection
     */
    public function getConnection()
    {
        if (!isset($this->connection)) {
            $this->connection = new mysqli(self::DB_SERVER, self::DB_USER, self::DB_PASSWD, self::DB_NAME, 3306);
        }

        if ($this->connection->connect_errno) {
            printf("Connect failed: %s\n", $this->connection->connect_error);
            exit();
        }
        return $this->connection;
    }


}
