<?php
/**
 * Created by IntelliJ IDEA.
 * User: ivo
 * Date: 27/03/2018
 * Time: 20:42
 */

use Jacwright\RestServer\RestServer;

require 'vendor/autoload.php';
require 'controller/APIController.php';
require 'service/DatabaseService.php';


$mode = 'debug'; // 'debug' or 'production'
$server = new RestServer($mode);
// $server->refreshCache(); // uncomment momentarily to clear the cache if classes change in production mode

// Bootstrap Controllers
$server->addClass('APIController');

$server->useCors = true;
$server->allowedOrigin = '*';

$server->handle();
